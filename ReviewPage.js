import React,{Component} from 'react'
import {Form,FormGroup,Button,Input} from 'reactstrap'
import { TiLocationArrow } from 'react-icons/ti';
import { FaMobileAlt } from 'react-icons/fa';
import emoji from './emoji.jpg';
import './style.css'
import axios from 'axios';
import Comments from './Comments'
class ReviewPage extends Component
{
    constructor(props)
    {
        super(props)
        this.state={
        name:'',
        comment:'',
        data:''
        }
    }
    onNameChange=(e)=>{
        this.setState({
            name:e.target.value
        })
    }
    onCommentChange=(e)=>
    {
        this.setState({
            comment:e.target.value
        })
    }
    SubmitValue = (e) => {
       
       
        if((this.state.name=='' || this.state.comment=='') ||(this.state.name=='' && this.state.comment==''))
        {
            alert("please fil all the fields correctly")
        }
    let formData={
             'name':this.state.name,
             'comment':this.state.comment
         }
         const body=JSON.stringify(formData)
         axios.post('https://our-saloon.herokuapp.com/api/post_comment',body,{headers:{
             'content-type':'application/json',
         }}).then((res)=>{console.log(res)
          
         })

           window.location.reload(false)
        
         e.preventDefault()
         
    
     }
    

    

    render()
    {
    
        return(
            <div>
        <Form>
            <FormGroup>
            <h1 id="rewh1">Say Something about React</h1>
            </FormGroup>
            <FormGroup>
                <Input id="ip" type="text" value={this.state.name} isSubmitted={true} onChange={this.onNameChange} placeholder="Your Name"/>
                <img id="img" src={emoji} alt="emoji"/>
                </FormGroup>
                <FormGroup>
                <FaMobileAlt id="icon1"/>
                    <textarea value="Your Comment" value={this.state.comment} isSubmitted={true} onChange={this.onCommentChange}/>
                    </FormGroup>
                    <FormGroup>
                    <TiLocationArrow id="icon2"/>
        <Button onClick={this.SubmitValue} id="btn">Comment</Button>
                        </FormGroup>
                        
            </Form>
            
            </div>
        )
    }
}
export default ReviewPage
