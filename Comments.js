import React,{Component} from 'react'
import Moment from 'react-moment';
import 'react-sticky-header/styles.css';
import StickyHeader from 'react-sticky-header';
 
import {Card,CardImg,CardBody,CardTitle,CardSubtitle,CardText} from 'reactstrap'
import emoji from './emoji.jpg'
import avatar1 from './avatar1.jpg'
import avatar2 from './avatar2.jpg'
import avatar3 from './avatar3.jpg'
import avatar4 from './avatar4.jpg'
import avatar5 from './avatar5.jpg'
import avatar6 from './avatar6.png'
import avatar7 from './avatar7.jpg'
import avatar8 from './avatar8.png'
import avatar9 from './avatar9.png'
import avatar10 from './avatar10.jpg'
import './style.css'
import axios from 'axios'
class Comments extends Component{
    constructor(props)
    {
        super(props);
        this.state={
            
            data:[],
            image:[avatar1,avatar2,avatar3,avatar4,avatar5,avatar6,avatar7,avatar8,avatar9,avatar10]
            
        }
        
    }
    componentDidMount()
    {
        axios.get(`https://our-saloon.herokuapp.com/api/get_comments`)
           .then(res => {
              console.log(res.data.user)
               this.setState({data:res.data.user});
             })
        
        // if(this.props.data)
        //  {
        //     console.log("props obtained")
        //      this.setState({data:this.props.data });
        //      console.log(this.props)
             
        
        //  }
        //  else{
        //     axios.get(`https://our-saloon.herokuapp.com/api/get_comments`)
        //     .then(res => {
        //       console.log(res.data.user)
        //       this.setState({data:res.data.user});
        //     })
        //     console.log(this.state.data)
            
        // }
        
            
    
    
        

    }

    
            render()
            
    {
        console.log(this.props.time)
         console.log(this.state.data.length)
                return (<div id="commdiv">
                    <StickyHeader
    // This is the sticky part of the header.
    header={
        <div id="sticheader"><span>{this.state.data.length}</span><h1 id="comh1">Comments</h1></div>
    }
  >
                   
                {
                    
                    this.state.data.map((item)=>{
                    const{name,comment,createdAt}=item
                    {console.log(createdAt)}
                    return(
                        
                        <div id="listdiv">
                        <ul id="un">
                    <li id="li">
                    <Card id="card1" col-lg-4  col-sm-12>
                      <CardImg id="img2" src={this.state.image[Math.floor(Math.random() * 10)]} alt="Card image cap" />
                       <CardBody>
                          <CardTitle>{name}</CardTitle>
                          <CardSubtitle><Moment fromNow>{createdAt}</Moment></CardSubtitle>
                          <CardText>{comment}</CardText>
                        </CardBody>
                      </Card>
                    </li>
                    
                </ul>
                </div>
                    )
                })}
                </StickyHeader>
                </div>
                )
                        }
                    
}
 export default Comments
